package cc.j1905.service.impl;

import cc.j1905.dao.CompanyWorkerMapper;
import cc.j1905.pojo.Companyworker;
import cc.j1905.pojo.Team;
import cc.j1905.service.CompanyWorkerService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class CompanyWorkerimpl implements CompanyWorkerService {
    @Resource
    CompanyWorkerMapper companyWorkerMapper;
    @Override
    public int getTotalRecordCount(Companyworker companyworker) {
        return companyWorkerMapper.getTotalRecordCount(companyworker);
    }

    @Override
    public List<Companyworker> getCompanyworkerByPage(Companyworker companyworker) {
        return companyWorkerMapper.getCompanyworkerByPage(companyworker);
    }

    @Override
    public List<Team> getDepartment() {
        return companyWorkerMapper.getDepartment();
    }



    @Override
    public int save(Companyworker companyworker) {
        return companyWorkerMapper.save(companyworker);
    }

    @Override
    public int delete(Integer id) {
        return companyWorkerMapper.delete(id);
    }

    @Override
    public int delectAll(String[] id) {
        return companyWorkerMapper.delectAll(id);
    }

    @Override
    public int update(Companyworker companyworker) {
        return companyWorkerMapper.update(companyworker);
    }

    @Override
    public Companyworker getOneById(int id) {
        return companyWorkerMapper.getOneById(id);
    }
}
