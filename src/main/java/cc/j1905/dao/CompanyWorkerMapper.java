package cc.j1905.dao;



import cc.j1905.pojo.Companyworker;
import cc.j1905.pojo.Team;

import java.util.List;

public interface CompanyWorkerMapper {

    //分页
    //得到总记录
    int getTotalRecordCount(Companyworker companyworker);
    //得到列表数据
    List<Companyworker> getCompanyworkerByPage(Companyworker companyworker);

    List<Team> getDepartment();


    //增加
    int save(Companyworker companyworker);
    // 删除
    int delete(Integer id);

    //批量删除
    int delectAll(String[] id);

    // 修改
    int update(Companyworker companyworker);

    Companyworker getOneById(int id);
}
