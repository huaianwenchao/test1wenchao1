package cc.j1905.controller;
import cc.j1905.pojo.Companyworker;
import cc.j1905.pojo.Team;
import cc.j1905.service.CompanyWorkerService;
import cc.j1905.util.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/companyworker")
public class CompanyWorkerController {
    @Resource
    CompanyWorkerService companyWorkerService;
//    @Resource
//    NewsService newsService;

    @RequestMapping("/list")
    public String list(Model model, Companyworker companyworker) {
        int totalRecordCount = companyWorkerService.getTotalRecordCount(companyworker);
        Page pageTool = new Page(totalRecordCount, companyworker.getCurrPageNo());
        companyworker.setPageSize(pageTool.getPageSize());
        companyworker.setStarNum(pageTool.getStartRow());
        List<Companyworker> list = companyWorkerService.getCompanyworkerByPage(companyworker);
        model.addAttribute("list", list);
        model.addAttribute("pageTool", pageTool);
        model.addAttribute("companyworker", companyworker);
//       //
        List<Team> team = companyWorkerService.getDepartment();
        model.addAttribute("team", team);
        return "/companyworker/list";
    }

    @RequestMapping("/delect")
    @ResponseBody       //Body里面的类容，返回文本
    public String delect(int id) {
        try {
            //判断是否有主从表关系，是否能删除

            if (companyWorkerService.delete(id) > 0) {
                return "1";
            } else {
                return "0";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "0";
    }

    @RequestMapping("/delectall")
    @ResponseBody       //Body里面的类容，返回文本@RequestParam(value = "id[]")声明一下是数组
    public String delectAll(@RequestParam(value = "id[]") String[] id) {
        //判断数组为空的情况，没选复选框
       /* if(id==null){
            System.out.println("d"+id);
            return "0";
        }*/
        //判断有没有类别不能删除的

        return companyWorkerService.delectAll(id) + "";//整形变成字符串
    }

//    增加（查询出所有类别用于页面展示）
    @RequestMapping("/add")
    public String add(Model model) {
        //查询得到所有类别
        List<Team> team = companyWorkerService.getDepartment();
        model.addAttribute("team", team);
        return "/companyworker/add";
    }
    @RequestMapping("/doadd")
    public String doAdd(Model model,Companyworker companyworker){
        System.out.println("----------doadd---------");
        try {
            if (companyWorkerService.save(companyworker)>0){
                return "redirect:/companyworker/list";
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:/companyworker/add";
    }
    @RequestMapping("/update")
    public String update(Model model,int id){
        System.out.println("----------update---------");
        Companyworker companyworker = companyWorkerService.getOneById(id);
        model.addAttribute("companyworker",companyworker);

        List<Team> team = companyWorkerService.getDepartment();
        model.addAttribute("team",team);
        return "/companyworker/update";
    }

    @RequestMapping("/doUpdate")
    public String doUpdate(Model model, Companyworker companyworker){
        try{
            if(companyWorkerService.update(companyworker)>0){
//                model.addAttribute("msg","<script>alert('修改数据成功！')</script>");
                model.addAttribute("msg","修改数据成功");

            }else {
                model.addAttribute("msg","修改数据失败");
            }
        }catch (Exception e){
            e.printStackTrace();
            model.addAttribute("msg"," 修改数据失败 ");
        }
        //重定向
        return "/companyworker/updatess";
    }
    @RequestMapping("/delete")
    @ResponseBody
    public String delete(Model model, int id){
        try{
            if (companyWorkerService.delete(id)>0){
                model.addAttribute("msg","删除成功");
                return "1";
            }else {
                model.addAttribute("msg","删除失败");
                return "0";
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return "0";
    }


    @RequestMapping("/deletebyid")
    public String deleteById(Model model,int id){
        try{
            if (companyWorkerService.delete(id)>0){
                model.addAttribute("msg","删除成功");
                return "forward:/talents/list";
            }else {
                model.addAttribute("msg","删除失败");
                return "forward:/companyworker/list";
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return "forward:/companyworker/list";

    }
}

