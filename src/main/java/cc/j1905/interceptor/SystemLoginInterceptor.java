package cc.j1905.interceptor;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SystemLoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //判断是否登录成功，没登录就转跳到登录页面
        HttpSession session=request.getSession();
        if(session.getAttribute("userInfo")==null){
            response.sendRedirect(request.getContextPath()+"/login");
        }
        return true;
    }
}
