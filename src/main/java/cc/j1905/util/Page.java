package cc.j1905.util;

import java.util.ArrayList;
import java.util.List;

public class Page {
    private Integer totalRecordCount;//总记录数
    private Integer pageSize=5;//每一页记录数
    private Integer pageCount;//共多少页
    private Integer currPageNo=1;//当前第几页
    private Integer num=3;//当前页 前后 各显示多少页


    /**
     * 获得当前页之前的页码集合
     * @return
     *
     * 分页跳页根据当前页显示前面几页
     */


    public List<Integer> getPrePages(){
        List<Integer> list=new ArrayList<Integer>();
        Integer frontStart=1;
        if(this.currPageNo>num){
            frontStart=this.currPageNo-num;
        }
        for(int i=frontStart;i<this.currPageNo;i++){
            list.add(i);
        }
        return list;
    }

    public List<Integer> getNextPages(){
        List<Integer> list=new ArrayList<Integer>();
        Integer frontEnd=this.num;
        if(this.pageCount!=null){
            if(num<pageCount&&currPageNo+num<pageCount){
                frontEnd=currPageNo+num;
            }else{
                frontEnd=pageCount;
            }
            for(int i=currPageNo+1;i<=frontEnd;i++){
                list.add(i);
            }
        }
        return list;

    }





    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Page(){
    }

    public Page(Integer totalRecordCount, Integer currPageNo){
        setTotalRecordCount(totalRecordCount);
        setCurrPageNo(currPageNo);

    }


    public Page(Integer totalRecordCount, Integer currPageNo, Integer pageSize){
        setPageSize(pageSize);

        setTotalRecordCount(totalRecordCount);
        setCurrPageNo(currPageNo);

    }


    public Integer getTotalRecordCount() {
        return totalRecordCount;
    }

    /**
     * 设置总记录数的同时 调用计算总页数的方法 setPageCountByRC
     * @param totalRecordCount
     */
    public void setTotalRecordCount(Integer totalRecordCount) {
        if(totalRecordCount>=0){
            this.totalRecordCount = totalRecordCount;
            setPageCountByRC(this.totalRecordCount );

        }




    }
    /**
     * 计算多少页
     * @param totalRecordCount
     */
    public void setPageCountByRC(Integer totalRecordCount){
        if(totalRecordCount%pageSize==0){
            pageCount=totalRecordCount/pageSize;

        }
        else{
            pageCount=totalRecordCount/pageSize+1;

        }
        if(pageCount==0){
            pageCount=1;
        }

    }






    public Integer getPageSize() {
        return pageSize;
    }
    private void setPageSize(Integer pageSize) {

        if(pageSize!=null&&pageSize>0){
            this.pageSize=pageSize;
        }


    }
    public Integer getPageCount() {
        return pageCount;
    }
    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }
    public Integer getCurrPageNo() {
        return currPageNo;
    }
    public void setCurrPageNo(Integer currPageNo) {
        if(currPageNo>this.pageCount){
            this.currPageNo=this.pageCount;
        }else if(currPageNo>0){
            this.currPageNo = currPageNo;

        }
        else{
            this.currPageNo =1;

        }





    }



    /**
     * 得到 起始行数
     * @return
     */
    public int getStartRow(){
        //return pageSize*(currPageNo-1)+1; // oracle
        return pageSize*(currPageNo-1);  //mysql
    }



    /**
     * 得到结尾行数
     * @return
     */
    public int getEndRow(){
        return pageSize*currPageNo;
    }


}
