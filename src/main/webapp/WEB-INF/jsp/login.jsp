<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/2/11
  Time: 15:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!doctype html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">

    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/admin/css/login.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/statics/admin/js/jquery.min.js"></script>
    <title>后台登陆</title>
</head>
<body>
<div id="login_top">
    <div id="welcome">
        欢迎使用教学质量与教学改革工程管理系统
    </div>
    <div id="back">
        <a href="#">返回首页</a>&nbsp;&nbsp; | &nbsp;&nbsp;
        <a href="#">帮助</a>
    </div>
</div>
<div id="login_center">
    <div id="login_area">
        <div id="login_form">
            <form action="${pageContext.request.contextPath}/dologin" method="post">
                <div id="login_tip">
                    用户登录&nbsp;&nbsp;UserLogin
                </div>
                <div><input type="text" name="userCode" class="username"></div>
                <div><input type="text" name="userPassword" class="pwd"></div>
                <div id="btn_area">
                    <input type="submit" name="submit" id="sub_btn" value="登&nbsp;&nbsp;录">&nbsp;&nbsp;
                    <span style="color: red">${error}</span>
                   <%-- <input type="text" class="verify">
                    <img src="${pageContext.request.contextPath}/statics/admin/images/login/verify.png" alt="" width="80" height="40">--%>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="login_bottom">
    版权所有
</div>
</body>
</html>