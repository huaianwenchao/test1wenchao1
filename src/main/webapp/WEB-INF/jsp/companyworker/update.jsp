<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/2/12
  Time: 9:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!doctype html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/admin/css/common.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/admin/css/main.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/statics/admin/js/colResizable-1.3.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/statics/admin/js/common.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/statics/My97DatePicker/WdatePicker.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/statics/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/statics/ckeditor/ckeditor.js"></script>

    <script type="text/javascript">
        $(function(){
            $(".list_table").colResizable({
                liveDrag:true,
                gripInnerHtml:"<div class='grip'></div>",
                draggingClass:"dragging",
                minWidth:30
            });

        });
    </script>
    <title>修改闻页</title>
</head>
<body>
<div class="container">

    <div id="forms" class="mt10">
        <div class="box">
            <div class="box_border">
                <div class="box_top"><b class="pl15">新增表单</b></div>
                <div class="box_center">
                    <form action="${pageContext.request.contextPath}/companyworker/doUpdate" enctype="multipart/form-data" method="post" class="jqtransform">
                        <table class="form_table pt15 pb15" width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="td_right">姓名：</td>
                                <td class="">
                                    <input type="text" value="${companyworker.name}"  name="name" class="input-text lh30" size="40">
                                </td>
                            </tr>
                            <tr>
                                <td class="td_right">工作年限：</td>
                                <td class="">
                                    <input type="text" value="${companyworker.workingLife}"  name="name" class="input-text lh30" size="40">
                                </td>
                            </tr>
                            <tr >
                                <td class="td_right">所属部门：</td>
                                <td class="">

                    <span class="fl">
                      <div class="select_border">
                        <div class="select_containers ">
                        <select name="departmentld" class="select">
                       <option value="">=请选择=</option>
                            <c:forEach items="${team}" var="item">
                                <option value="${item.id}">${item.department}</option>
                            </c:forEach>
                        </select>
                        </div>
                      </div>
                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_right">毕业学校：</td>
                                <td class="">
                                    <input type="text" value="${companyworker.graduateSchool}"  name="name" class="input-text lh30" size="40">
                                </td>
                            </tr>
                            <tr>
                                <td class="td_right">个人简介：</td>
                                <td class="">
                                    <textarea name="personalProfile"  cols="30" rows="10" class="textarea">${companyworker.personalProfile}</textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_right">工作经历：</td>
                                <td class="">
                                    <textarea name="workExperience"  cols="30" rows="10" class="textarea">${companyworker.workExperience}</textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_right">&nbsp;</td>
                                <td class="">
                                    <input type="submit" name="button" class="btn btn82 btn_save2" value="保存">
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

