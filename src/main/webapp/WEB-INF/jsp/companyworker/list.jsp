<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
 <!doctype html>
 <html lang="zh-CN">
 <head>
   <meta charset="UTF-8">
     <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/admin/css/common.css">
     <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/admin/css/main.css">
     <script type="text/javascript" src="${pageContext.request.contextPath}/statics/admin/js/colResizable-1.3.min.js"></script>
     <script type="text/javascript" src="${pageContext.request.contextPath}/statics/admin/js/common.js"></script>

     <script type="text/javascript" src="${pageContext.request.contextPath}/statics/My97DatePicker/WdatePicker.js"></script>
     <script type="text/javascript" src="${pageContext.request.contextPath}/statics/js/jquery-3.3.1.min.js"></script>
     <script type="text/javascript" src="${pageContext.request.contextPath}/statics/ckeditor/ckeditor.js"></script>
   
   <script type="text/javascript">
      $(function(){  
        $(".list_table").colResizable({
          liveDrag:true,
          gripInnerHtml:"<div class='grip'></div>", 
          draggingClass:"dragging", 
          minWidth:30
        }); 
        
      }); 
   </script>
   <title>新闻类别列表页</title>
     <script>
         function delectNewsType(id) {
             if(!confirm("数据一旦删除无法恢复，你确定要删除吗？")){
                 return;
             }
             $.get("${pageContext.request.contextPath}/companyworker/delect",//路径4
                 {id:id,r:Math.random()},//表单需要的id;r:Math.random()防止缓存问题
                 function (result) {//回调函数
                     if(result=="1"){
                         alert("删除数据成功");
                     }else{
                         alert("删除数据失败");
                     }
                     window.location=window.location;
                 });
         }

         /*选中下面的复选框*/
         function selectAll() {
             //在当前文档里面通过name属性得到所有这个属性
             var cbAll=document.getElementsByName("cbAll")[0];
             var cbItems=document.getElementsByName("cbItem");
             for(var i=0;i<cbItems.length;i++){
                 cbItems[i].checked=cbAll.checked;
             }
         }
         /*批量删除*/
         function delectAllNewsTypeByids() {
             //先把下面选中的id放到一个数组里面
             if(!confirm("数据一旦删除无法恢复，你确定要删除吗？")){
                 return;
             }
             //用来存放将要删除的所有的id
             var ids=new Array();/*长度可变的集合*/
             var chItems=document.getElementsByName("cbItem"); /*包含复选框*/
             for(var i=0;i<chItems.length;i++){
                 //如果选中就放到ids这个数组里面去（checked这个方法是复选框选中）
                 if(chItems[i].checked){
                     ids.push(chItems[i].value);
                 }
             }
             if(ids.length<=0){
                 alert("批量删除操作，至少选中一个要删除项！")
                 return;
             }
             //异步访问删除数据(项目跟目录)id由控制器传过来的值决定
             $.get("${pageContext.request.contextPath}/companyworker/delectall",{id:ids},function (result) {
                 if(result=="-1"){
                     alert("您要删除的内别中有子类别，删除失败");
                 }else if(result=="0"){
                     alert("批量删除操作失败");
                 }else {
                     alert("批量删除数据成功！");
                 }
             });
             window.location=window.location;
         }
     </script>
 </head>
 <body>
  <div class="container">
      <form method="get" action="${pageContext.request.contextPath}/companyworker/list">
    <div id="search_bar" class="mt10">
       <div class="box">
          <div class="box_border">
            <div class="box_top"><b class="pl15">搜索</b></div>
            <div class="box_center pt10 pb10">
              <table class="form_table" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>部门:</td>
                  <td>
                    <span class="fl">
                      <div class="select_border">
                        <div class="select_containers ">
                         <select name="departmentld" class="select">
                                <option value="">==请选择==</option>
                                      <c:forEach items="${team}" var="item">
                                          <option value="${item.id}">${item.department}</option>
                                      </c:forEach>
                        </select>
                        </div>
                      </div>
                    </span>
                  </td>
                </tr>
              </table>
            </div>
            <div class="box_bottom pb5 pt5 pr10" style="border-top:1px solid #dadada;">
              <div class="search_bar_btn" style="text-align:right;">
                  <input type="submit" value="搜索" class="ext_btn ext_btn_submit">
                  <a href="${pageContext.request.contextPath}/companyworker/add" class="ext_btn"><span class="add"></span>添加</a>
                  <input type="button" value="批量删除" onclick="delectAllNewsTypeByids()" class="ext_btn">
              </div>
            </div>
          </div>
        </div>
    </div>
      </form>
     <div id="table" class="mt10">
        <div class="box span10 oh">
              <table width="100%" border="0" cellpadding="0" cellspacing="0" class="list_table">
                <tr>
                  <th class="td_center" width="30"><input type="checkbox" name="cbAll" onclick="selectAll()"/></th>
                    <td>编号</td>
                    <td>姓名</td>
                    <td>工作年限</td>
                    <td>所属部门</td>
                    <td>毕业学校</td>
                    <td>操作</td>
              </tr>
                  <c:forEach var="item" items="${list}">
                      <tr class="tr">
                          <td><input class="td_center" type="checkbox"  name="cbItem" value="${item.id}"></td>
                          <td>${item.id}</td>
                          <td>${item.name}</td>
                          <td>${item.workingLife}</td>
                          <td>${item.department}</td>
                          <td>${item.graduateSchool}</td>
                          <td><a href="javascript:" onclick="delectNewsType(${item.id})">删除</a><a href="${pageContext.request.contextPath}/companyworker/update?id=${item.id}">编辑</a></td> <%--加这个会执行javascript的语句--%>
                      </tr>
                  </c:forEach>
              </table>
              <div class="page mt10">
                <div class="pagination">
                  <ul>
                      <li class="first-child"><a href="${pageContext.request.contextPath}/companyworker/list?currPageNo=1&departmentld=${companyworker.departmentld}">首页</a></li>
                      <li ><a href="${pageContext.request.contextPath}/companyworker/list?currPageNo=${pageTool.currPageNo-1}&departmentld=${companyworker.departmentld}">上一页</a></li>
                      <li><a href="${pageContext.request.contextPath}/companyworker/list?currPageNo=${pageTool.currPageNo+1}&departmentld=${companyworker.departmentld}">下一页</a></li>
                      <li class="last-child"><a href="${pageContext.request.contextPath}/companyworker/list?currPageNo=${pageTool.pageCount}&departmentld=${companyworker.departmentld}">末页</a></li>
                  </ul>
                </div>
              </div>
        </div>
     </div>
   </div>
  ${msg}
 </body>
 </html>
  